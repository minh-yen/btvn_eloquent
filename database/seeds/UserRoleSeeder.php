<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\RoleUser;
use App\Phone;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('id', 1)->first();

        Phone::created([
        	'number' => (int)'012345678',
        	'user_id' => $user->id
        ]);

        $arrRoles = ['create', 'update', 'delete'];
        
        foreach ($arrRoles as $arrRole) {
        	$role = Role::create([
        		'name' => $arrRole
        	]);

        	$role_user = RoleUser::create([
        		'user_id' => $user->id,
        		'role_id' => $role->id
        	]);
        }


    }
}
