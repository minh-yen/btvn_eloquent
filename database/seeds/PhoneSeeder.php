<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Phone;

class PhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach ($users as $user) {
        	do {
        		$rand = 0;

        		for ($i = 0; $i < 9; $i++) {
        			$rand .= rand(0, 9);
        		}

        		$phone = Phone::where('number', $rand)->first();
        		
        	} while ($phone != null);

        	Phone::create([
        		'number' => $rand,
        		'user_id' => $user->id
        	]);
        }
    }
}
