<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$data = array(
    		array('name'=>'Yến', 'first_name'=>'Phùng', 'last_name' => 'Thị', 'class'=>'AT13D', 'email' => 'yen@gmail.com', 'password' => Hash::make('123123')),
    		array('name'=>'Khương', 'first_name'=>'Phan', 'last_name' => 'Xuân', 'class'=>'AT13D', 'email' => 'khuong@gmail.com', 'password' => Hash::make('123123')),
    		array('name'=>'Nam', 'first_name'=>'Nguyễn', 'last_name' => 'Thành', 'class'=>'AT13E', 'email' => 'nam@gmail.com', 'password' => Hash::make('123123')),
    		array('name'=>'Tùng', 'first_name'=>'Nguyễn', 'last_name' => 'Văn', 'class'=>'AT13A', 'email' => 'tung@gmail.com', 'password' => Hash::make('123123')),
    		array('name'=>'Đức', 'first_name'=>'Bùi', 'last_name' => 'Văn', 'class'=>'AT13F', 'email' => 'duc@gmail.com', 'password' => Hash::make('123123'))
    	);
        DB::table('users')->insert($data);
    }
}
