<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('user');
})->name('view_search');

Route::post('/getUser', 'UserController@search')->name('search');

Route::get('/search-role', 'RoleController@index')->name('view-search-role');
Route::post('/get-user-role', 'RoleController@search')->name('search-role');