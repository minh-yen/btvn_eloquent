<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
	@if ($status == 'error')
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ $message }}</strong>
		</div>
	@else 
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ $message }}</strong>
		</div>
		
		@if (isset($datas['infoUsers']))
			<h1>Tìm kiếm theo User ID</h1>
			<br>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>id</th>
							<th>name</th>
							<th>class</th>
							<th>role</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{ $datas['infoUsers']->id }}</td>
							<td>{{ $datas['infoUsers']->name }}</td>
							<td>{{ $datas['infoUsers']->class }}</td>
							<td>
								@foreach ($datas['infoUsers']->roles as $role)
								<span class="label" style="padding: 5px; background: green;">{{ $role->name }}</span>
								@endforeach
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		@endif

		@if (isset($datas['infoPhone']))
			<h1>Tìm kiếm theo Phone Number</h1>
			<br>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>id</th>
							<th>name</th>
							<th>class</th>
							<th>number</th>
							<th>role</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>{{ $datas['infoPhone']->info->id }}</td>
							<td>{{ $datas['infoPhone']->info->name }}</td>
							<td>{{ $datas['infoPhone']->info->class }}</td>
							<td>{{ $datas['infoPhone']->number }}</td>
							<td>
								@foreach ($datas['infoPhone']->info->roles as $role)
									<span class="label" style="padding: 5px; background: green;">{{ $role->name }}</span>
								@endforeach
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		@endif

		@if (isset($datas['infoRoles']))
			<h1>Tìm kiếm theo Role Name</h1>
			<br>
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>id</th>
							<th>name</th>
							<th>class</th>
							<th>role</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($datas['infoRoles']->users as $user)
							<tr>
								<td>{{ $user->id }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->class }}</td>
								<td>
									<span class="label" style="padding: 5px; background: green;">{{ $datas['infoRoles']->name }}</span>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@endif
	@endif

	<a href="{{ route('view-search-role') }}" class="btn btn-info">Quay lại</a>
</body>
</html>