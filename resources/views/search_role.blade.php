
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Tìm kiếm user và role</title>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
	@if (session('status'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ session('status') }}</strong>
		</div>
	@endif
	<form action="{{ route('search-role') }}" method="POST" role="form">
		@csrf
		
		<legend>Tìm kiếm theo role, user và phone</legend>
	
		<div class="form-group">
			<label for="">User ID</label>
			<input type="text" class="form-control" id="" name="id" placeholder="Input field">
		</div>

		<div class="form-group">
			<label for="">Phone</label>
			<input type="text" class="form-control" id="" name="phone" placeholder="Input field">
		</div>
		
		<div class="form-group">
			<label for="">Role Name</label>
			<input type="text" class="form-control" id="" name="name" placeholder="Input field">
		</div>
	
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
</body>
</html>