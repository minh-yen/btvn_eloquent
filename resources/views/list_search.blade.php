<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
	@if ($status == 'error')
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ $message }}</strong>
		</div>
	@else 
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ $message }}</strong>
		</div>

		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>STT</th>
						<th>id</th>
						<th>name</th>
						<th>class</th>
					</tr>
				</thead>
				<tbody>
					@php
						$STT = 0;
					@endphp
					@foreach ($datas as $data)
					<tr>
						<td>{{ $STT + 1 }}</td>
						<td>{{ $data->id }}</td>
						<td>{{ $data->name }}</td>
						<td>{{ $data->class }}</td>
					</tr>
					@php
						$STT++;
					@endphp
					@endforeach
				</tbody>
			</table>
		</div>
	@endif

	<a href="{{ route('view_search') }}" class="btn btn-info">Quay lại</a>
</body>
</html>