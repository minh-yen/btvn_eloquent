<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Phone;
use App\RoleUser;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        return view('search_role');
    }


    public function search(Request $request) {
        if ($request->id != null || $request->name != null || $request->phone != null) {

            $infos = [];
            if ($request->id != null) {
                $user = User::where('id', $request->id)
                            ->with('roles')
                            ->first();
                if ($user != null) {
                    
                    $infos['infoUsers'] = $user; 
                }
            }

            if ($request->phone != null) {
                $user = Phone::where('number', $request->phone)
                            ->first();
                if ($user != null) {
                    
                    $infos['infoPhone'] = $user->userRoles($user->user_id, $user); 
                }
            }

            if ($request->name != null) {

                $user = Role::where('name', $request->name)
                                        ->with('users')
                                        ->first();
                if ($user != null) {
                    
                    $infos['infoRoles'] = $user; 
                }
                 
            }
            
            if (count($infos) > 0) {
                return view('list_search_role', [
                    'status'    => 'success',
                    'message'   => 'Tìm thấy ' . count($infos) . ' bản ghi',
                    'datas'     => $infos
                ]);
            } else {
                return view('list_search_role', [
                    'status'    => 'error',
                    'message'   => 'Không tìm thấy thông tin tương thích!'
                ]);
            }
        } else {
            return redirect()->back()->with('status', 'bạn chưa chọn thông tin cần tìm!');
        }
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
