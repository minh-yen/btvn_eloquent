<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User ;
class UserController extends Controller
{
     public function search(Request $request) {
     	if ($request->id != null || $request->name != null || $request->class != null) {
     		$users = User::select('id', 'name', 'class')
			     		->where('id',$request->id)
			     		->orWhere('name',$request->name)
			     		->orWhere('class',$request->class)
			     		->orderBy('id','DESC')
			     		->get();
     		
     		if (count($users) > 0) {
     			return view('list_search', [
     				'status'	=> 'success',
     				'message' 	=> 'Tìm thấy ' . count($users) . ' bản ghi',
     				'datas' 	=> $users
     			]);
     		} else {
     			return view('list_search', [
     				'status'	=> 'error',
     				'message' 	=> 'Không tìm thấy thông tin tương thích!'
     			]);
     		}
     	} else {
     		return redirect()->back()->with('status', 'bạn chưa chọn thông tin cần tìm!');
     	}
     }

}
