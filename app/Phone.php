<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Phone extends Model
{
    protected $table = 'phones';
    protected $fillable = ['id', 'number', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function userRoles($id, $user){
    	$user->info = User::where('id', $id)->with('roles')->first();
    	return $user;
    }
}
